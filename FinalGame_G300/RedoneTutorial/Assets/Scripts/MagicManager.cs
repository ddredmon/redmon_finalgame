﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagicManager : MonoBehaviour
{
    public Slider magicSlider;
    public PlayerMovement playerMovement;
    // Start is called before the first frame update
    void Start()
    {
        magicSlider.maxValue = playerMovement.maxMagic;
        magicSlider.value = playerMovement.maxMagic;
        playerMovement.currentMagic = playerMovement.maxMagic;
    }
    public void AddMagic()
    {
        magicSlider.value += 2;
        playerMovement.currentMagic += 2;
        if (magicSlider.value > magicSlider.maxValue)
        {
            magicSlider.value = magicSlider.maxValue;
            playerMovement.currentMagic = playerMovement.maxMagic;
        }
    }
    public void DecreaseMagic()
    {
        magicSlider.value -= 1;
        playerMovement.currentMagic -= 1;
        if(magicSlider.value < 0)
        {
            magicSlider.value = 0;
            playerMovement.currentMagic = 0;
        }
    }
  
}
